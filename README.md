﻿# Desafio Frontend - Bseller/Esmart - BIT SP

## Instruções

- Para apenas visualizar o projeto acesse a pasta src e abra o index.html.
- Caso queira fazer alterações no arquivo .scss terá que fazer os passos abaixo.
- Instalar o Node JS (caso não tenha) https://nodejs.org/en/
- Ao instalar o Node JS, virá junto com ele o NPM (pacote de gerenciamento de módulos).
- No prompt de comando entre no diretório do projeto e digite:
- npm install + enter
- gulp default + enter
- Fazendo isso, sempre que salvar o scss, automaticamente irá compilá-lo para css e o browser terá sincronia instantânea caso hajam alterações nos arquivos html, scss e js.
- Para minificar os arquivos css e js digite:
- gulp build
- Caso queira utilizar os arquivos minificados, terá que alterar o caminho no index.html.