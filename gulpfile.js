// gulp
var gulp = require('gulp');

// plugins
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var pump = require('pump');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

/*
  -- FUNÇÕES --
  gulp.task - Define a task
  gullp.src - Aponta para arquivos fontes
  gulp.dest - Aponta para a pasta de saída
  gulp.watch - Observa mudanças feitas em pastas e arquivos
*/

// *** tasks *** //

//Compilar Sass
gulp.task('sass', function () {
  return gulp.src('./src/sass/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./src/css'));
});

//Observa as mudanças no Sass
gulp.task('sass:watch', function () {
  gulp.watch('./src/sass/*.scss', ['sass']);
});

//Minificando CSS
gulp.task('minify-css', function () {
    gulp.src('./src/css/*.css')
        .pipe(cssmin())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('src/css'));
});

//Minificando JS
gulp.task('compress', function () {
  return gulp.src(['./src/js/*.js'])
    .pipe(uglify())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('src/js'));
});

//Alterar arquivos e ao salvar o navegador dá reload automaticamente
gulp.task('browser-sync', function() {
  browserSync({
    server: {
      baseDir: 'src'
    }
  });
  gulp.watch(['*.html', 'css/**/*.css', 'js/**/*.js'], {cwd: 'src'}, reload);
});

// *** default task *** //
gulp.task('default', function() {
  runSequence(
    ['sass','sass:watch','browser-sync']
  );
});

// *** build task *** //
gulp.task('build', function() {
  runSequence(
    ['minify-css','compress']
  );
});
