jQuery.getJSON('js/data.json',function(json){	
	var html = "";
	json['best-sellers'].forEach(function(item){

	$('.best-sellers').append(jQuery.parseHTML('<li><section class="card"><a href="" title=""><figure class="product-figure"><img src="'+item.image+'" class="product-image" alt="" width="220" height="220"></figure><span class="product-title">'+item.title+'</span></a> <div class="card-price-container"><a href="" title="" class="card-text-section"><p class="card-price"><span class="price-sell">'+item.price.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})+'</span> <span class="installment">'+item.installments.number+' x de <span class="installment-value">'+item.installments.value.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})+'</span></span></p></a></div><a class="card-shipping">Frete Grátis</a><a class="card-buy-btn">Comprar<span class="icon-card-buy-btn"></span></a></section></li>'));

	});
	json['releases'].forEach(function(item){
		$('.releases').append(jQuery.parseHTML('<li><section class="card"><a href="" title=""><figure class="product-figure"><img src="'+item.image+'" class="product-image" alt="" width="220" height="220"></figure><span class="product-title">'+item.title+'</span></a> <div class="card-price-container"><a href="" title="" class="card-text-section"><p class="card-price"><span class="price-sell">'+item.price.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})+'</span> <span class="installment">'+item.installments.number+' x de <span class="installment-value">'+item.installments.value.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})+'</span></span></p></a></div><a class="card-shipping">Frete Grátis</a><a class="card-buy-btn">Comprar<span class="icon-card-buy-btn"></span></a></section></li>'));
	});

	montarSlick();
});